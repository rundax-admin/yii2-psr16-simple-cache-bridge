<?php

namespace Yii2Extended\Yii2SimpleCache;

/**
 * Psr16ToYii2SimpleCache class file.
 * 
 * This class transforms psr-16 compliant simple cache calls to yii2 compliant
 * cache calls.
 * 
 * @author Anastaszor
 */
class Psr16ToYii2SimpleCache implements \Psr\SimpleCache\CacheInterface
{
	
	/**
	 * The yii2 compliant cache.
	 * 
	 * @var \yii\caching\CacheInterface
	 */
	protected $_yii2_cache = null;
	
	/**
	 * Builds a new Psr16ToYii2SimpleCache object with the given inner psr-16
	 * simple cache.
	 * 
	 * @param \yii\caching\CacheInterface $cache
	 */
	public function __construct(\yii\caching\CacheInterface $cache)
	{
		$this->_yii2_cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::get()
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		return $this->_yii2_cache->get($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::set()
	 * @return boolean
	 */
	public function set($key, $value, $ttl = null)
	{
		return $this->_yii2_cache->set($key, $value, $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::delete()
	 * @return boolean
	 */
	public function delete($key)
	{
		return $this->_yii2_cache->delete($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::clear()
	 * @return boolean
	 */
	public function clear()
	{
		return $this->_yii2_cache->flush();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::getMultiple()
	 * @return mixed[]
	 */
	public function getMultiple($keys, $default = null)
	{
		return $this->_yii2_cache->multiGet($keys);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::setMultiple()
	 * @return boolean
	 */
	public function setMultiple($values, $ttl = null)
	{
		return [] !== $this->_yii2_cache->multiSet($values, $ttl);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::deleteMultiple()
	 * @return boolean
	 */
	public function deleteMultiple($keys)
	{
		$res = true;
		foreach($keys as $key)
			$res |= $this->_yii2_cache->delete($key);
		return $res;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\SimpleCache\CacheInterface::has()
	 * @return boolean
	 */
	public function has($key)
	{
		return $this->_yii2_cache->exists($key);
	}
	
}
