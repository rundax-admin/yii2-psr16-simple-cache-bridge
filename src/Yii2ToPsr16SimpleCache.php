<?php

namespace Yii2Extended\Yii2SimpleCache;

/**
 * Yii2ToPsr16SimpleCache class file.
 * 
 * This class transforms yii2 compliant cache calls to psr-16 compliant simple
 * cache calls.
 * 
 * @author Anastaszor
 */
class Yii2ToPsr16SimpleCache implements \yii\caching\CacheInterface
{
	
	/**
	 * The psr-16 compliant cache.
	 * 
	 * @var \Psr\SimpleCache\CacheInterface
	 */
	protected $_psr16_cache = null;
	
	/**
	 * Builds a new Yii2ToPsr16SimpleCache object with the given inner yii2 
	 * cache.
	 * 
	 * @param \Psr\SimpleCache\CacheInterface $cache
	 */
	public function __construct(\Psr\SimpleCache\CacheInterface $cache)
	{
		$this->_psr16_cache = $cache;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::buildKey()
	 * @return string
	 */
	public function buildKey($key)
	{
		switch(gettype($key))
		{
			case 'boolean':
			case 'bool':
			case 'integer':
			case 'int':
			case 'double':
			case 'float':
			case 'string':
				$strkey = (string) $key;
				if(preg_match('#^[A-Za-z0-9_-]{1,32}$#', $strkey))
					return $strkey;
				return md5($key);
			case 'array':
				return md5(implode(':', array_keys($key)));
			case 'object':
				return md5(spl_object_hash($key).serialize($key));
			case 'resource':
				return md5(get_resource_type($key));
			default:
				return md5('');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::get()
	 * @return mixed
	 */
	public function get($key)
	{
		return $this->_psr16_cache->get($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::exists()
	 * @return boolean
	 */
	public function exists($key)
	{
		return $this->_psr16_cache->has($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiGet()
	 * @return mixed
	 */
	public function multiGet($keys)
	{
		return $this->_psr16_cache->getMultiple($keys);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::set()
	 * @return boolean
	 */
	public function set($key, $value, $duration = null, $dependency = null)
	{
		return $this->_psr16_cache->set($key, $value, $duration);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiSet()
	 * @return string[]
	 */
	public function multiSet($items, $duration = 0, $dependency = null)
	{
		$failed = [];
		foreach($items as $key => $item)
		{
			$res = $this->set($key, $item, $duration, $dependency);
			if(!$res)
				$failed[] = $key;
		}
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::add()
	 * @return boolean
	 */
	public function add($key, $value, $duration = 0, $dependency = null)
	{
		if(!$this->exists($key))
			$this->set($key, $value, $duration, $dependency);
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::multiAdd()
	 * @return string[]
	 */
	public function multiAdd($items, $duration = 0, $dependency = null)
	{
		$failed = [];
		foreach($items as $key => $item)
		{
			$res = $this->add($key, $item, $duration, $dependency);
			if(!$res)
				$failed[] = $key;
		}
		return $failed;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::delete()
	 * @return boolean
	 */
	public function delete($key)
	{
		return $this->_psr16_cache->delete($key);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::flush()
	 * @return boolean
	 */
	public function flush()
	{
		return $this->_psr16_cache->clear();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \yii\caching\CacheInterface::getOrSet()
	 */
	public function getOrSet($key, $callable, $duration = null, $dependency = null)
	{
		if($this->exists($key))
			return $this->get($key);
		$value = $callable();
		if($value !== false)
			$this->set($key, $value, $duration, $dependency);
		return $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetExists()
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
		return $this->exists($offset);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetGet()
	 * @return mixed
	 */
	public function offsetGet($offset)
	{
		return $this->get($offset);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetSet()
	 * @return boolean
	 */
	public function offsetSet($offset, $value)
	{
		return $this->set($offset, $value);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \ArrayAccess::offsetUnset()
	 * @return boolean
	 */
	public function offsetUnset($offset)
	{
		return $this->delete($offset);
	}
	
}
